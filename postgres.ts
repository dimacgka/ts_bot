import { Pool } from 'pg';

const pool = new Pool({
  connectionString: process.env.POSTGRESQL_URL,
});

export async function saveResults(url, result) {
  const client = await pool.connect();

  try {
    await client.query('BEGIN');

    // проверка на уникальность
    const { rows } = await client.query(
      'SELECT * FROM results WHERE url = $1 AND created_at >= $2',
      [url, new Date().setHours(0, 0, 0, 0)],
    );

    if (rows.length > 0) {
      console.log(`URL "${url}" has already been processed today`);
      return;
    }

    // вставка результатов в таблицу
    await client.query(
      'INSERT INTO results (url, cookies, content_width, content_height) VALUES ($1, $2, $3, $4)',
      [url, JSON.stringify(result.cookies), result.contentWidth, result.contentHeight],
    );

    await client.query('COMMIT');
  } catch (error) {
    await client.query('ROLLBACK');
    throw error;
  } finally {
    client.release();
  }
}

export async function isUrlProcessedToday(url) {
  const client = await pool.connect();

  try {
    const { rows } = await client.query(
      'SELECT * FROM results WHERE url = $1 AND created_at >= $2',
      [url, new Date().setHours(0, 0, 0, 0)],
    );

    return rows.length > 0;
  } catch (error) {
    throw error;
  } finally {
    client.release();
  }
}
// import amqp from 'amqplib';

// export async function connectToQueue() {
//   const urlRabbit = process.env.RABBITMQ_URL;

//   if (!urlRabbit) {
//     throw new Error('RabbitMQ URL is not defined');
//   }

//   const queueConnection = await amqp.connect(urlRabbit);
//   return queueConnection;
// }

// export async function consumeFromQueue(queue: any, callback) {
//   await queue.assertQueue(process.env.RABBITMQ_QUEUE);
//   queue.consume(process.env.RABBITMQ_QUEUE, (message: amqp.ConsumeMessage | null) => {
//     if (!message) {
//       return;
//     }

//     const url = message.content.toString();
//   });
// }
// import amqp, { Connection, Channel, ConsumeMessage } from 'amqplib';

// export async function connectToQueue(): Promise<Connection> {
//   const urlRabbit: string | undefined = process.env.RABBITMQ_URL;

//   if (!urlRabbit) {
//     throw new Error('RabbitMQ URL is not defined');
//   }

//   const queueConnection: Connection = await amqp.connect(urlRabbit);
//   return queueConnection;
// }

// export async function consumeFromQueue(queue: Channel, callback: (message: string) => void): Promise<void> {
//   await queue.assertQueue(process.env.RABBITMQ_QUEUE);
//   queue.consume(process.env.RABBITMQ_QUEUE, (message: ConsumeMessage | null) => {
//     if (!message) {
//       return;
//     }

//     const url: string = message.content.toString();
//     callback(url);
//   });
// }

import amqp, { Connection, Channel, ConsumeMessage } from 'amqplib';

export async function connectToQueue(): Promise<Connection> {
  const urlRabbit = process.env.RABBITMQ_URL;

  if (!urlRabbit) {
    throw new Error('RabbitMQ URL is not defined');
  }

  const queueConnection = await amqp.connect(urlRabbit);
  return queueConnection;
}

export async function consumeFromQueue(queue: Channel, callback: (url: string) => void): Promise<void> {
  const queueName = process.env.RABBITMQ_QUEUE;

  if (!queueName) {
    throw new Error('Queue name is not defined');
  }

  await queue.assertQueue(queueName);
  queue.consume(queueName, (message: ConsumeMessage | null) => {
    if (!message) {
      return;
    }

    const url = message.content.toString();
    callback(url);
  });
}
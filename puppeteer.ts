import puppeteer from 'puppeteer-core';

export async function fetchPageContent(url) {
  const browser = await puppeteer.launch({ args: ['--no-sandbox'] });
  const page = await browser.newPage();

  await page.goto(url, { waitUntil: 'networkidle0' });

  const cookies = await page.cookies();
  const contentWidth = await page.evaluate(() => document.documentElement.clientWidth);
  const contentHeight = await page.evaluate(() => document.documentElement.clientHeight);

  await browser.close();

  return { cookies, contentWidth, contentHeight };
}
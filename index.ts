import { connectToQueue, consumeFromQueue } from './rabbit';
import { fetchPageContent } from './puppeteer';
import { saveResults, isUrlProcessedToday } from './postgres'
import amqp from 'amqplib';


(async () => {
  // подключение к очереди
  const queueConnection = await connectToQueue();
  const queue = await queueConnection.createChannel();

  // обработка сообщений из очереди
  await consumeFromQueue(queue, async (message: amqp.ConsumeMessage) => {
    const url = message.content.toString();

    // проверка на уникальность
    if (await isUrlProcessedToday(url)) {
      console.log(`URL "${url}" has already been processed today`);
      return;
    }

    // загрузка страницы и извлечение информации
    const result = await fetchPageContent(url);

    // сохранение результатов в БД
    await saveResults(url, result);

    // подтверждение обработки сообщения из очереди
    queue.ack(message);
  });
})();